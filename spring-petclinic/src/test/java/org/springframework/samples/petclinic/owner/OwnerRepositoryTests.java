package org.springframework.samples.petclinic.owner;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class OwnerRepositoryTests {

	private Owner owner;
	
	@Autowired
	private OwnerRepository ownerRepository;
	
	@Before
	public void init() {
		
		if(this.owner==null){
			owner = new Owner();

			
			owner.setAddress("Direcci�n de prueba");
			owner.setCity("Ciudad de prueba");
			owner.setFirstName("Nombre prueba");
			owner.setLastName("Apellido prueba");
			owner.setTelephone("123456789");
		}
		
		this.ownerRepository.save(owner);
	}
	
	@Test
	public void testDeleteById() {
		int id = owner.getId();
		assertNotNull(ownerRepository.findById(id));

		ownerRepository.deleteById(id);
		assertNull(ownerRepository.findById(id));
	}
	@After
	public void finish() {
		this.owner=null;
	}

}
