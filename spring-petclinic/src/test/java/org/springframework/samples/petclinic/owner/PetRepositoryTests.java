package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.service.EntityUtils;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTests {

	@Autowired
	private PetRepository pets;
	
	@Autowired
	private OwnerRepository ownerRepository;
	
	private Pet pet;
	       
	
	@Before
	public void init() {
		if(this.pet==null) {
					
			pet = new Pet();
			pet.setName("Mascota de prueba");
			pet.setWeight(2);
			pet.setComments("Comentarios de prueba");
			pet.setBirthDate(LocalDate.now());
			Collection<PetType> types = this.pets.findPetTypes();
		    pet.setType(EntityUtils.getById(types, PetType.class, 2));
		    
		    pet.setOwner(ownerRepository.findById(1));
			
		    
		}
		this.pets.save(pet);
	}
	
	@Test
	public void deleteTest() {
		int id = pet.getId();
		assertNotNull(pets.findById(id));
		pets.delete(pet);
		assertNull(pets.findById(id));
	}
	@Test
	public void findPetsByNameTest() {
		Collection<Pet> petColl = new LinkedList<Pet>();
		petColl.add(pet);
		assertEquals(petColl,pets.findPetsByName(pet.getName()));
	}
	@After
	public void finish() {
		this.pet=null;
	}

}
