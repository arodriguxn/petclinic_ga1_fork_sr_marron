package org.springframework.samples.petclinic.owner;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;

public class PetTests {
	public static Pet pet;
	
	@BeforeClass
	public static void initClass() {
		pet = new Pet();
	}
	
	@Test
	public void testPet() {
		assertNotNull(pet);
	}
	
	@Test
	public void testSetGetWeight() {
		float weight = 5;
		pet.setWeight(weight);
		assertNotNull(pet.getWeight());
		assertEquals("El valor del peso no se ha introducido correctamente", weight, pet.getWeight(), 0);
	}
	
	@Test
	public void testSetGetComments() {
		String comentarios = "Este comentario es un comentario de prueba";
		pet.setComments(comentarios);
		assertNotNull(pet.getComments());
		assertTrue(pet.getComments().equals(comentarios));
	}

}
