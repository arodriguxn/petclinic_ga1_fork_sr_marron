package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class VetTests {
	
	public static Vet vet;
	public static Vet vet2;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		vet = new Vet();
		vet.setFirstName("Juan Manuel");
		vet.setLastName("Murillo");
	}
	
	@Before
	//This test is executed before each test created in this suite
	public void init() {
		vet.setHomeVisits(false);
	}
	
	@Before
	public void initVisits() {
		vet.setVisitsInternal(null);
	}
	
	@Test
	public void testVet() {
		assertNotNull(vet);				
	}
	
	@Test
	public void testVet2() {
		assertNull(vet2);				
	}
	
	@Test
	public void testFirstName() {
		assertNotNull(vet.getFirstName());
		assertNotEquals(vet.getFirstName(), "JuanMa");
		assertEquals(vet.getFirstName(), "Juan Manuel");		
	}
	
	@Test
	public void testLastName() {
		assertNotNull(vet.getLastName());
		assertEquals(vet.getLastName(), "Murillo");
		assertNotEquals(vet.getLastName(), "Rodríguez");		
	}
	
	@Test
	public void testHomeVisits() {
		vet.setHomeVisits(true);
		assertNotNull(vet.getHomeVisits());
		assertTrue(vet.getHomeVisits());
		
		vet.setHomeVisits(false);
		assertFalse(vet.getHomeVisits());
	}
	
    @Test
    public void testSerialization() {
    	Vet vet3 = new  Vet();
        vet3.setFirstName("Zaphod");
        vet3.setLastName("Beeblebrox");
        vet3.setId(123);
        Vet other = (Vet) SerializationUtils
                .deserialize(SerializationUtils.serialize(vet3));
        assertThat(other.getFirstName()).isEqualTo(vet3.getFirstName());
        assertThat(other.getLastName()).isEqualTo(vet3.getLastName());
        assertThat(other.getId()).isEqualTo(vet3.getId());
    }
    
    @Test
    public void addVisitTest() {
    	
    	List<Visit> list = new LinkedList<Visit>();
    	Visit visit = new Visit();
    	visit.setDate(LocalDate.now());
    	visit.setDescription("Descripcion");
    	visit.setId(888);
    	visit.setPetId(1);
    	visit.setVet(vet);
    	
    	//List is empty
    	assertEquals(list,vet.getVisits());
    	
    	vet.addVisit(visit);
    	list.add(visit);
    	
	assertNotNull(vet.getVisits());
    	assertEquals(list,vet.getVisits());
    }
    
	@Ignore
	//This test is not executed
	public void ignore() {
		vet.setFirstName("JuanMa");
	}
	
	@Test
	public void testSetGetInternalVisits() {
		//Se comprueba que getVisitsInternal devuelve un conjunto de visitas que no es nulo
		assertNotNull(vet.getVisitsInternal());
		//Se comprueba que getVisitsInternal devuelve un conjunto de visitas vacio, pues no hay ninguno de momento
		assertTrue(vet.getVisitsInternal().isEmpty());
		
		//Se declara un nuevo conjunto de visitas
		Set <Visit> visits = new HashSet<Visit>();
		//Se declara una nueva variable de tipo Visita
		Visit visit_1 = new Visit();
		//Se introduce una descripcion a visit_1
		visit_1.setDescription("Esta es la visita 1, creada antes que la vistia 2");
		//Se introduce un identificador a visit_1 para "identificarla"
		visit_1.setId(10);
		//Se declara una nueva variable de tipo Visita
		Visit visit_2 = new Visit();
		//Se introduce una descripcion a visit_2
		visit_2.setDescription("Esta es la visita 2, creada después que la visita 1");
		//Se introduce un identificador a visit_2 para "identificarla"
		visit_2.setId(12);
		//Se introduce visit_2 en el conjunto de visitas declarado
		visits.add(visit_2);
		//Se introduce visit_2 en el conjunto de visitas declarado
		visits.add(visit_1);
		//Mediante setVisitsInternal, asignamos el conjunto de visitas al veterinario
		vet.setVisitsInternal(visits);
		//Comprobamos que setVisitsInternal ha funcionado correctamente comprobando que getVisitsInternal devuelve un conjunto no vacio
		assertFalse(vet.getVisitsInternal().isEmpty());
	}
	
	@Test
	public void testGetVisits() {
		//Se comprueba que getVisits devuelve un conjunto que no es vacio
		assertNotNull(vet.getVisits());
		//Se comprueba que getVisits devuelve un conjunto de visitas no vacio, pues en el anterior metodo de prueba ya se introdujeron valores
		assertTrue(vet.getVisits().isEmpty());
		
		//Se declara un nuevo conjunto de visitas
		Set <Visit> visits = new HashSet<Visit>();
		//Se declara una nueva variable de tipo Visita
		Visit visit_1 = new Visit();
		//Se introduce una descripcion a visit_1
		visit_1.setDescription("Esta es la visita 1, creada antes que la vistia 2");
		//Se introduce un identificador a visit_1 para "identificarla"
		visit_1.setId(10);
		//Se declara una nueva variable de tipo Visita
		Visit visit_2 = new Visit();
		//Se introduce una descripcion a visit_2
		visit_2.setDescription("Esta es la visita 2, creada después que la visita 1");
		//Se introduce un identificador a visit_2 para "identificarla"
		visit_2.setId(12);
		//Se declara una nueva variable de tipo Visita
		Visit visit_3 = new Visit();
		//Se introduce una descripcion a visit_2
		visit_3.setDescription("Esta es la visita 3, creada después que la visita 2");
		//Se introduce un identificador a visit_2 para "identificarla"
		visit_3.setId(15);
		//Se introduce visit_2 en el conjunto de visitas declarado
		visits.add(visit_2);
		//Se introduce visit_2 en el conjunto de visitas declarado
		visits.add(visit_1);
		//Se introduce visit_3 en el conjunto de visitas declarado
		visits.add(visit_3);
		//Se asigna el conjunto visits al veterinario
		vet.setVisitsInternal(visits); 
		
		//Se comprueba que el conjunto de visitas no es nulo, que posee 3 elementos y que estos tampoco son nulos
		assertNotNull(vet.getVisits());
		assertTrue(vet.getVisits().size() == 3);
		assertNotNull(vet.getVisits().get(0));
		assertNotNull(vet.getVisits().get(1));
		assertNotNull(vet.getVisits().get(2));
	}
	
	@Test
	public void testAddVisit() {
		//Se crea e inicializa un nuevo objeto de tipo visita
		Visit visit = new Visit(); 
		visit.setDescription("Esta visita sera insertada por el metodo del veterinario");
		visit.setId(20);
		//Se comprueba que el tamanno original del conjunto de visitas de nuestro veterinario es 2
		assertTrue(vet.getVisitsInternal().size() == 0);
		//Se annade la nueva visita
		vet.addVisit(visit);
		//Tras annadir la nueva visita, el conjunto debe haber incrementado en una unidad
		assertTrue(vet.getVisitsInternal().size() == 1);
		
	}
    
    @After
    //This test is executed after each test created in this suite
    public void finish() {
    	vet.setHomeVisits(true);
    }
    
    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	vet = null;
    }
}
